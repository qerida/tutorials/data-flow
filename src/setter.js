import { LitElement, html } from '@polymer/lit-element'

class SetterElement extends LitElement {

  // declare properties used in _render
  static get properties() {
    return {
      value: Number,
      min: Number,
      max: Number
    }
  }

  constructor () {
    super()
    // set default values
    this.min = 10
    this.max = 50
    this.value = 30
  }

  // invoked when slider changes
  sliderChanged(event) {
    this.value = event.currentTarget.value
    this.dispatchEvent(new CustomEvent('value-changed', {bubbles: true, composed: true, detail: this.value}))
  }

  _render(props) {
    return html`
<h2>Hello from setter</h2>
<p>props: ${JSON.stringify(props)}</p>   
<input id="slider" type="range" min="${props.min}" max="${props.max}" value="${props.value}" oninput="${(e) => this.sliderChanged(e)}">
`
  }
}

customElements.define('setter-element', SetterElement)
