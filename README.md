# data-flow

In this project we will create three elements :

- getter-element: takes a number ob balloons and displays them
- setter element: contains a slider and will emit an Event on value change
- container-element: holds the two elements and passes the value

The following six steps are tagged in git. Use `git checkout <tagname>` to see the repo state at that step.
- `01-container` - [diff](https://gitlab.com/qerida/tutorials/data-flow/commit/51b8e135125ce252a94083fd30e9a8b82a1a391e)
- `02-getter-setter` - [diff](https://gitlab.com/qerida/tutorials/data-flow/compare/01-container...02-getter-setter)
- `03-setter-properties` - [diff](https://gitlab.com/qerida/tutorials/data-flow/compare/02-getter-setter...03-setter-properties)
- `04-slider` - [diff](https://gitlab.com/qerida/tutorials/data-flow/compare/03-setter-properties...04-slider)
- `05-balloons` - [diff](https://gitlab.com/qerida/tutorials/data-flow/compare/04-slider...05-balloons)
- `05-events` - [diff](https://gitlab.com/qerida/tutorials/data-flow/compare/05-balloons...06-events)

# 01 - container-element

First we create the container element. This is quite simple. Just follow the simple-element tutorial

# 02 - getter-setter

Here we create two new elements. The getter (src/getter.js) and the setter (src/setter.js) element. Still very simple elements. To use them in the container, we import the files and use them in the _render method of the container.

# 03 - setter-properties

This step is a showcase for element properties.

As you can see properties are declared in `static get properties()`. This method returns an object that describes the properties. Initial values are set in the constructor of the class. Note that we don't forget to call the constructor of the superclass (`super()`).

The declared properties are passed to the _render method:

`src/setter.js`
```js
  _render(props) {
    return html`
        <h2>Hello from setter</h2>
        <p>${JSON.stringify(props)}</p>
    `
```

Note: You can use [Object destructuring](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment) to access the properties in the render method directly:

```js
  _render({initValue, tmp}) {
    return html`
        <h2>Hello from setter</h2>
        <p>initValue: ${initValue}</p>
        <p>tmp: ${tmp}</p>
    `
```

# 04 - slider

Here we implement the slider. Our setter-element takes min, max and value as properties and pass them to an input of type range (slider). Min max and value have default values if they are not set in the container. 

Additionally we trigger a method when the slider value changes to update the state of the element

`/src/setter.js`
```js
  _render(props) {
    return html`
        <h2>Hello from setter</h2>
        <p>props: ${JSON.stringify(props)}</p>   
        <input id="slider" type="range" min="${props.min}" max="${props.max}" value="${props.value}" oninput="${this.sliderChanged.bind(this)}">
    `
  }

  sliderChanged() { this.value = this._root.getElementById('slider').value }
```

# 05 - balloons

Time to implement the getter. The getter takes a Number of balloons to display. We apply a bit of styling to our component:

`src/getter.js`
```js
class GetterElement extends LitElement {

  static get properties() { return { balloons: Number } }

  _render(props) {
    return html`
        <style>
            .balloonclass {
                color: red;
                font-size: 3em;
            }
        </style>

        <h2>Hello from getter</h1>
        <p>No. of balloonns: ${props.balloons}</p>
        <span class="balloonclass">${'🎈 '.repeat(props.balloons)}</span>
    `
  }
}
```

# 06 - events

Now lets connect the elements. Ideally we could use two way databinding from polymer to achieve this. However this is not implemented in lit-element. Two way databinding brings a lot of complexity to a project and can make state management quite hardly comprehensible. So we will use custom events to notify the container when changes happen.

The affected method is sliderChanged. We only need to add one line:

`src/setter.js`
```js
this.dispatchEvent(new CustomEvent('value-changed', {bubbles: true, composed: true, detail: this.value}))
```

In the container we add the logic to react to the event:

`src/container.js`
```js
  _firstRendered() {
    this.addEventListener('value-changed', (e) => { this.shadowRoot.querySelector('getter-element').balloons = e.detail})
  }
```
